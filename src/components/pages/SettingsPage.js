import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as handleShowing from '../../redux/actions/MyPageActions';
import { Form, Checkbox, FormGroup, Col, ControlLabel } from 'react-bootstrap';
import PropTypes from 'prop-types';

function MyPagePage(props) {
        const { doesShow } = props.myPage;
        const { handleShowing } = props.actions;

        return (
            <Form horizontal style={{fontSize: '20px'}}>
                <FormGroup>
                    <Col componentClass={ControlLabel} sm={7}>Отображать выполненные задачи?</Col>
                    <Col sm={3}>
                        {doesShow ? (
                            <Checkbox onChange={ e => handleShowing(e.target.checked)} defaultChecked/>
                        ) : (
                            <Checkbox onChange={ e => handleShowing(e.target.checked)}/>
                        )}
                    </Col>
                </FormGroup>
            </Form>
        )
}

MyPagePage.proptypes = {
    myPage: PropTypes.bool.isRequired,
    actions: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        myPage: state.myPage
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(handleShowing, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPagePage)
