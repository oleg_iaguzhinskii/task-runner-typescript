import React from 'react'
import LoginForm from '../LoginForm'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {login, handleLoginText} from '../../redux/actions/LoginFormActions'
import {setOpenedPage} from '../../redux/actions/NavActions'


function LoginPage(props) {
    const {errorMessage, isLogged, isLoginTextInvalid} = props.data;

    return (
        <LoginForm actions={props.actions} errorMessage={errorMessage} isLogged={isLogged}
                   isLoginTextInvalid={isLoginTextInvalid}/>
    )
}

function mapStateToProps(state) {
    return {
        data: state.loginForm
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({login, handleLoginText, setOpenedPage}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)

