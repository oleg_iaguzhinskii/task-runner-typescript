import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { openCommonModalWindow, closeCommonModalWindow } from '../../redux/actions/TasksActions'
import { sort } from '../../redux/actions/SortingActions'
import Sorting from '../Sorting'
import Tasks from '../Tasks'
import ModalWindow from '../ModalWindow'

class CommonTasksPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        const self = this;

        setTimeout(function() {
            fetch('http://localhost:4000/tasks')
                .then(function (response) {
                    return response.json();
                })
                .then(function (tasks) {
                    const tasksList = tasks.filter(t => t.isPrivate === false);

                    self.setState({
                        commonTasks: tasksList
                    });
                })
                .catch(function (ex) {
                    console.log('parsing failed: ' + ex);
                });
        }, 1000);
    }

    render() {
        const { actions } = this.props;
        const { commonTasks } = this.state;
        const { openedCommonModalWindow } = this.props.data;
        const { sortBy, descending } = this.props.sortData;
        const { doesShow } = this.props.myPage;

        if (commonTasks) {
            commonTasks.sort((a, b) => descending
                ? a[sortBy] < b[sortBy]
                : a[sortBy] > b[sortBy]
            );
        }

        return (
            <div>
                <Sorting actions={actions} sortBy={sortBy} descending={descending}/>
                <Tasks tasksList={commonTasks} openModalWindow={actions.openCommonModalWindow} doesShow={doesShow}/>
                <ModalWindow tasksList={commonTasks} openedModalWindow={openedCommonModalWindow}
                    closeModalWindow={actions.closeCommonModalWindow}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state.tasks,
        sortData: state.sorting,
        myPage: state.myPage
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ openCommonModalWindow, closeCommonModalWindow, sort }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommonTasksPage)