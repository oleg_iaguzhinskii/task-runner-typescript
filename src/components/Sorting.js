import React from 'react'
import {ButtonToolbar, Button, Col} from 'react-bootstrap'
import styles from '../../styles/components/sorting.css'

export default function Sorting(props) {
    const { sortBy, descending} = props;
    const { sort } = props.actions;
    const sorting = [
        {
            name: 'date',
            val: 'По дате',
            id: 1
        },
        {
            name: 'tag',
            val: 'По приоритету',
            id: 2
        },
        {
            name: 'isComplete',
            val: 'По выполнению',
            id: 3
        },
        {
            name: 'category',
            val: 'По категории',
            id: 4
        }
    ];

    return (
        <Col xsOffset={2} mdOffset={3}>
            <ButtonToolbar className={styles.sorting}>
                {sorting.map(function (item) {
                    let text = item.val;
                    if (sortBy === item.name) {
                        text += descending ? ' \u2191' : ' \u2193'
                    }
                    return (
                        <Button className={styles.sortButton} key={item.id} id={item.id} bsStyle="primary"
                                bsSize="large"
                                onClick={e => sort(Number(e.target.id), sortBy, descending)}>
                            {text}
                        </Button>
                    )
                })}
            </ButtonToolbar>
        </Col>
    )
}
