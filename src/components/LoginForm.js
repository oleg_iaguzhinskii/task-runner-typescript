import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col } from 'react-bootstrap'

function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleLoginText(text) {
        this.setState({login: text});
        this.props.actions.handleLoginText(text);
    }

    onSubmit(e) {
        e.preventDefault();
        let { login, password } = this.state;
        this.props.actions.login(login, password);
    }

    render() {
        const { errorMessage, isLogged, isLoginTextInvalid } = this.props;
        const { setOpenedPage } = this.props.actions;
        const button = (errorMessage && isLoginTextInvalid) ? (
            <Button bsStyle="primary" type="submit" disabled>Войти</Button>
        ) : (
            <Button bsStyle="primary" type="submit">Войти</Button>
        );

        return (
            <form onSubmit={::this.onSubmit}>
                <Col mdOffset={3}  md={6}>
                    <FieldGroup id="formControlsText" type="text" label="Логин" placeholder='Логин' autoFocus
                            onChange={e => ::this.handleLoginText(e.target.value)}/>
                    {isLogged ? (
                        <Redirect to='/my_tasks' onClick={setOpenedPage('/my_tasks')}/>
                    ) : (
                        <div style={{fontSize: '0.7em', color: 'red'}}>{errorMessage}</div>
                    )}
                </Col>
                <Col mdOffset={3}  md={6}>
                    <FieldGroup id="formControlsPassword" type="password" label="Пароль"
                            onChange={ e => this.setState( {password: e.target.value} )} placeholder='Пароль' />
                </Col>
                <Col mdOffset={3}  md={6}>
                    <div>{button}</div>
                </Col>
            </form>
        )
    }
}