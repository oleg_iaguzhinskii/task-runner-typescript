import React from 'react'
import styles from '../../styles/components/modalWindow.css'
import getDate from '../utils/getDate'
import {Modal, Button, Row, Col} from 'react-bootstrap'

export default function ModalWindow(props) {
    const {tasksList, openedModalWindow, closeModalWindow, completeTheTask, page} = props;
    const {login, id, title, category, subcategory, date, dateTill, description, isComplete} =
        tasksList && openedModalWindow ? tasksList.filter(t => t.id === openedModalWindow)[0]
            : {
                login: '', id: 0, title: '', category: '', subcategory: '',
                date: null, dateTill: undefined, description: '', isComplete: false
            };

    const modalWindow = tasksList && openedModalWindow ? (
        <div key={id} id={id} className="static-modal">
            <Modal.Dialog>
                <Modal.Header>
                    <Modal.Title><Row>
                        <Col xs={8}>{title}</Col>
                        {page === undefined && (
                            <Col xs={4} className={styles.byWhom}>created by&nbsp;
                                {login[0].toUpperCase() + login.slice(1)}</Col>
                        )}
                    </Row></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row className={styles.groupAndDate}>
                        <Col xs={6} className={styles.inline}>
                            <p className={styles.category}>{category}</p>
                            <p className={styles.subcategory}>{subcategory}</p>
                        </Col>
                        <Col xs={6} className={styles.inline}>
                            <p>
                                    <span className={styles.end}>До:&nbsp;
                                        {(dateTill === 'Бессрочно' || (dateTill && dateTill.length === 10) )
                                            ? dateTill
                                            : getDate(new Date(dateTill))
                                        }
                                </span>
                            </p>
                            <p className={styles.start}>От:&nbsp;
                                {getDate(new Date(date))}</p>
                        </Col>
                    </Row>
                    <div className={styles.content}>
                        {description}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    {page === '/my_tasks' ? (
                        isComplete ? (
                            <Button bsStyle="success" disabled>Выполнена</Button>
                        ) : (
                            <Button bsStyle="success"
                                    onClick=
                                        {e => completeTheTask(
                                            Number(e.target.parentNode.parentNode.parentNode.parentNode.parentNode.id)
                                        )}
                            >Завершить</Button>
                        )
                    ) : null}
                    <Button onClick={closeModalWindow}>Закрыть</Button>
                </Modal.Footer>
            </Modal.Dialog>
        </div>
    ) : (
        null
    );

    return (
        modalWindow
    )
}


