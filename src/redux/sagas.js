import * as constants from './constants/loginForm';
import {loginPending} from './actions/LoginFormActions';

import {call, put, takeLatest, all} from 'redux-saga/effects';

function fetchDate(login, password) {
    if (login && password) {
        login = login.toLowerCase();
        password = password.toLowerCase();
    }
    return function(dispatch) {
        dispatch(loginPending());

        return fetch('http://localhost:4000/users')
            .then(function (response) {
                return response.json();
            })
            .then(function (users) {
                let user = users.filter(u => u.login === login)[0];

                if (user && user.password === password) {
                    dispatch(loginSuccess(user));
                } else {
                    dispatch(loginError('Неправильно введён логин или пароль'));
                }
            })
            .catch(function (ex) {
                console.log('parsing failed: ' + ex);
            });
    }
}

export function* login() {
    try {
        const a = yield call(fetchDate);
        yield put({type: constants.LOGIN_PENDING});
    } catch {

    }

}

export function* watchHelloSaga() {
    yield takeLatest(constants.LOGIN_PENDING, login);
}

export default function* rootSaga() {
    yield all([
        watchHelloSaga()
    ])
}
