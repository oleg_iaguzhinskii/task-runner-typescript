import { combineReducers } from 'redux'
import nav from './nav.tsx'
import loginForm from './loginForm.tsx'
import tasks from './tasks.tsx'
import sorting from './sorting.tsx'
import createCategory from './createCategory.ts'
import createTask from './createTask.tsx'
import myPage from './myPage.tsx'

export default combineReducers({
    nav,
    loginForm,
    tasks,
    sorting,
    createTask,
    createCategory,
    myPage
})
