import { SHOW_THE_COMPLETED_TASKS, HIDE_THE_COMPLETED_TASKS} from '../constants/myPage'
import { MyPageStoreState } from '../../types/StoreState'

const initialState = {
    doesShow: false
};

export default function myPage(state: MyPageStoreState = initialState, action): MyPageStoreState {
    switch (action.type) {
        case SHOW_THE_COMPLETED_TASKS:
            return { ...state, doesShow: true };
        case HIDE_THE_COMPLETED_TASKS:
            return { ...state, doesShow: false };
        default:
            return state;
    }
}
