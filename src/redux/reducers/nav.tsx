import { SET_OPENED_PAGE } from '../constants/nav'
import { NavStoreState } from '../../types/StoreState'

const initialState = {
    page: '/'

};

export default function nav(state: NavStoreState = initialState, action): NavStoreState {
    switch (action.type) {
        case SET_OPENED_PAGE:
            return { ...state, page: action.payload.page };
        default:
            return state;
    }
}
