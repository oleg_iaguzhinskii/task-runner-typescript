import { SORT_TASK } from '../constants/sorting'
import { SortingStoreState } from '../../types/StoreState'

const initialState = {
    sortBy: 'date',
    descending: false
};

export default function sorting(state: SortingStoreState = initialState, action): SortingStoreState {
    switch (action.type) {
        case SORT_TASK:
            return { ...state, sortBy: action.payload.sortBy, descending: action.payload.descending };
        default:
            return state;
    }
}
