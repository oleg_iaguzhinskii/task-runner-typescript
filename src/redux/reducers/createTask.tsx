import * as constants from '../constants/createTask'
import { CreateTaskStoreState } from '../../types/StoreState'

const initialState = {
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    isPrivate: false,
    category: undefined,
    subcategory: undefined,
    dateTill: '',
    hoursToRemind: 12,
    typeWordOfHours: 'часов',
    tag: 0,
    errorMessage: null
};

export default function createTask(state: CreateTaskStoreState = initialState, action): CreateTaskStoreState {
    switch (action.type) {
        case constants.EMPTY_FIELD:
            return { ...state, isFieldEmpty: true, errorMessage: action.payload.errorMessage };
        case constants.NOT_EMPTY_FIELD:
            return { ...state, isFieldEmpty: false, errorMessage: null };
        case constants.SET_TEXT_TITLE_OF_TASK:
            return { ...state, textTitle: action.payload.textTitle };
        case constants.SET_TEXT_TEXT_AREA_OF_TASK:
            return { ...state, textTextArea: action.payload.textTextArea };
        case constants.SET_PRIVATE:
            return { ...state, isPrivate: true };
        case constants.DELETE_PRIVATE:
            return { ...state, isPrivate: false };
        case constants.SET_CATEGORY:
            return { ...state, category: action.payload.category };
        case constants.SET_SUBCATEGORY:
            return { ...state, subcategory: action.payload.subcategory };
        case constants.SET_DATE_TILL:
            return { ...state, dateTill: action.payload.dateTill };
        case constants.SET_HOURS_TO_REMIND:
            return { ...state, hoursToRemind: action.payload.hoursToRemind,
                typeWordOfHours: action.payload.typeWordOfHours };
        case constants.SET_TAG:
            return { ...state, tag: action.payload.tag, errorMessage: action.payload.errorMessage };
        case constants.RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
