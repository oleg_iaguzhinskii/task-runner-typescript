import * as constants from '../constants/createCategory'
import { CreateCategoryStoreState } from '../../types/StoreState'

const initialState = {
    typeOfCreation: 'category',
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    category: undefined,
    subcategory: undefined,
    errorMessage: null
};

export default function createCategory(state: CreateCategoryStoreState = initialState, action): CreateCategoryStoreState {
    switch (action.type) {
        case constants.SET_TYPE_OF_CREATION:
            return { ...state, typeOfCreation: action.payload.typeOfCreation}
        case constants.EMPTY_FIELD:
            return { ...state, isFieldEmpty: true, errorMessage: action.payload.errorMessage };
        case constants.NOT_EMPTY_FIELD:
            return { ...state, isFieldEmpty: false, errorMessage: null };
        case constants.SET_TEXT_TITLE:
            return { ...state, textTitle: action.payload.textTitle };
        case constants.SET_TEXT_TEXT_AREA:
            return { ...state, textTextArea: action.payload.textTextArea };
        case constants.SET_CATEGORY:
            return { ...state, category: action.payload.category };
        case constants.SET_SUBCATEGORY:
            return { ...state, subcategory: action.payload.subcategory };
        case constants.RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
