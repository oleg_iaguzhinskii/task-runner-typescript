import * as constants from '../constants/createTask';
const initialState = {
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    isPrivate: false,
    category: undefined,
    subcategory: undefined,
    dateTill: '',
    hoursToRemind: 12,
    typeWordOfHours: 'часов',
    tag: 0,
    errorMessage: null
};
export default function createTask(state = initialState, action) {
    switch (action.type) {
        case constants.EMPTY_FIELD:
            return Object.assign({}, state, { isFieldEmpty: true, errorMessage: action.payload.errorMessage });
        case constants.NOT_EMPTY_FIELD:
            return Object.assign({}, state, { isFieldEmpty: false, errorMessage: null });
        case constants.SET_TEXT_TITLE_OF_TASK:
            return Object.assign({}, state, { textTitle: action.payload.textTitle });
        case constants.SET_TEXT_TEXT_AREA_OF_TASK:
            return Object.assign({}, state, { textTextArea: action.payload.textTextArea });
        case constants.SET_PRIVATE:
            return Object.assign({}, state, { isPrivate: true });
        case constants.DELETE_PRIVATE:
            return Object.assign({}, state, { isPrivate: false });
        case constants.SET_CATEGORY:
            return Object.assign({}, state, { category: action.payload.category });
        case constants.SET_SUBCATEGORY:
            return Object.assign({}, state, { subcategory: action.payload.subcategory });
        case constants.SET_DATE_TILL:
            return Object.assign({}, state, { dateTill: action.payload.dateTill });
        case constants.SET_HOURS_TO_REMIND:
            return Object.assign({}, state, { hoursToRemind: action.payload.hoursToRemind, typeWordOfHours: action.payload.typeWordOfHours });
        case constants.SET_TAG:
            return Object.assign({}, state, { tag: action.payload.tag, errorMessage: action.payload.errorMessage });
        case constants.RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
