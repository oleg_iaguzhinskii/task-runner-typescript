import * as constants from '../constants/createCategory';
const initialState = {
    typeOfCreation: 'category',
    isFieldEmpty: true,
    textTitle: '',
    textTextArea: '',
    category: undefined,
    subcategory: undefined,
    errorMessage: null
};
export default function createCategory(state = initialState, action) {
    switch (action.type) {
        case constants.SET_TYPE_OF_CREATION:
            return Object.assign({}, state, { typeOfCreation: action.payload.typeOfCreation });
        case constants.EMPTY_FIELD:
            return Object.assign({}, state, { isFieldEmpty: true, errorMessage: action.payload.errorMessage });
        case constants.NOT_EMPTY_FIELD:
            return Object.assign({}, state, { isFieldEmpty: false, errorMessage: null });
        case constants.SET_TEXT_TITLE:
            return Object.assign({}, state, { textTitle: action.payload.textTitle });
        case constants.SET_TEXT_TEXT_AREA:
            return Object.assign({}, state, { textTextArea: action.payload.textTextArea });
        case constants.SET_CATEGORY:
            return Object.assign({}, state, { category: action.payload.category });
        case constants.SET_SUBCATEGORY:
            return Object.assign({}, state, { subcategory: action.payload.subcategory });
        case constants.RESET_FORM:
            return initialState;
        default:
            return state;
    }
}
