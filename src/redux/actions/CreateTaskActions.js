import * as constants from '../constants/createTask';
export function setFieldEmpty() {
    return {
        type: constants.EMPTY_FIELD,
        payload: {
            errorMessage: 'Поля не могут быть пустыми!'
        }
    };
}
export function onChangeField() {
    return {
        type: constants.NOT_EMPTY_FIELD
    };
}
export function setTextTitleOfTask(text) {
    return {
        type: constants.SET_TEXT_TITLE_OF_TASK,
        payload: {
            textTitle: text
        }
    };
}
export function setTextTextAreaOfTask(text) {
    return {
        type: constants.SET_TEXT_TEXT_AREA_OF_TASK,
        payload: {
            textTextArea: text
        }
    };
}
export function handlePrivacy(checked) {
    if (!checked) {
        return {
            type: constants.DELETE_PRIVATE
        };
    }
    else {
        return {
            type: constants.SET_PRIVATE
        };
    }
}
export function selectCategory(category, subcategory) {
    selectSubcategory(subcategory);
    return {
        type: constants.SET_CATEGORY,
        payload: {
            category: category
        }
    };
}
export function selectSubcategory(subcategory) {
    return {
        type: constants.SET_SUBCATEGORY,
        payload: {
            subcategory: subcategory
        }
    };
}
export function selectDateTill(date) {
    if (!(date && date.length > 0)) {
        date = 'Бессрочно';
    }
    return {
        type: constants.SET_DATE_TILL,
        payload: {
            dateTill: date
        }
    };
}
export function setHoursToRemind(value) {
    let text = '';
    // const type1 = [1, 21, 31, 41]; //"час"
    // const type2 = [2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43 , 44]; //"часа"
    switch (Number(value)) {
        case 1:
        case 21:
        case 31:
        case 41:
            text = 'час';
            break;
        case 2:
        case 3:
        case 4:
        case 22:
        case 23:
        case 24:
        case 32:
        case 33:
        case 34:
        case 42:
        case 43:
        case 44:
            text = 'часа';
            break;
        default:
            text = 'часов';
            break;
    }
    return {
        type: constants.SET_HOURS_TO_REMIND,
        payload: {
            hoursToRemind: value,
            typeWordOfHours: text
        }
    };
}
export function setTag(tag) {
    return {
        type: constants.SET_TAG,
        payload: {
            tag: tag,
            errorMessage: tag === 0 ? 'Неправильный тег!' : null
        }
    };
}
export function resetForm() {
    return {
        type: constants.RESET_FORM
    };
}
