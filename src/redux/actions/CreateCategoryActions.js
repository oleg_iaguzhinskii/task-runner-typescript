import * as constants from '../constants/createCategory';
export function setTypeOfCreation(type) {
    if (type === 'category') {
        return {
            type: constants.SET_TYPE_OF_CREATION,
            payload: {
                typeOfCreation: 'category'
            }
        };
    }
    else if (type === 'subcategory') {
        return {
            type: constants.SET_TYPE_OF_CREATION,
            payload: {
                typeOfCreation: 'subcategory'
            }
        };
    }
}
export function setFieldEmpty() {
    return {
        type: constants.EMPTY_FIELD,
        payload: {
            errorMessage: 'Поля не могут быть пустыми!'
        }
    };
}
export function onChangeField() {
    return {
        type: constants.NOT_EMPTY_FIELD
    };
}
export function setTextTitle(text) {
    return {
        type: constants.SET_TEXT_TITLE,
        payload: {
            textTitle: text
        }
    };
}
export function setTextTextArea(text) {
    return {
        type: constants.SET_TEXT_TEXT_AREA,
        payload: {
            textTextArea: text
        }
    };
}
export function selectCategory(category, subcategory) {
    selectSubcategory(subcategory);
    return {
        type: constants.SET_CATEGORY,
        payload: {
            category: category
        }
    };
}
export function selectSubcategory(subcategory) {
    return {
        type: constants.SET_SUBCATEGORY,
        payload: {
            subcategory: subcategory
        }
    };
}
export function resetForm() {
    return {
        type: constants.RESET_FORM
    };
}
