import * as constants from '../constants/sorting'
import * as actions from '../../types/SortingActions'

export function sort(column, sortBy, descending): actions.Sort {
    switch (column) {
        case 1:
            column = 'date';
            break;
        case 2:
            column = 'tag';
            break;
        case 3:
            column = 'isComplete';
            break;
        case 4:
            column = 'category';
            break;
        default:
            column = 'tag';
    }
    descending = sortBy === column && !descending;

    return {
        type: constants.SORT_TASK,
        payload: {
            sortBy: column,
            descending: descending
        }
    }
}
