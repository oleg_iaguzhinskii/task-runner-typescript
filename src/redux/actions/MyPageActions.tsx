import * as constants from '../constants/myPage'
import * as actions from '../../types/MyPageActions'

export function handleShowing(doesShow): actions.HandleShowing {
    if (doesShow) {
        return {
            type: constants.SHOW_THE_COMPLETED_TASKS
        }
    } else {
        return {
            type: constants.HIDE_THE_COMPLETED_TASKS
        }
    }
}
