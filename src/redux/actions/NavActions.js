import * as constantsLoginForm from '../constants/loginForm';
import * as constantsNav from '../constants/nav';
export function setOpenedPage(page) {
    return {
        type: constantsNav.SET_OPENED_PAGE,
        payload: {
            page: page
        }
    };
}
export function logout() {
    return {
        type: constantsLoginForm.LOG_OUT
    };
}
