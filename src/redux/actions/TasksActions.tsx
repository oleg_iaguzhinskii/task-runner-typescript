import * as constants from '../constants/tasks'
import * as actions from '../../types/TasksActions'

export function openCommonModalWindow(id): actions.OpenedCommonModalWindow {
    return {
        type: constants.OPEN_COMMON_MODAL_WINDOW,
        payload: {
            id: id
        }
    }
}

export function openMyModalWindow(id): actions.OpenMyModalWindow {
    return {
        type: constants.OPEN_MY_MODAL_WINDOW,
        payload: {
            id: id
        }
    }
}

export function closeCommonModalWindow(): actions.CloseCommonModalWindow {
    return {
        type: constants.CLOSE_COMMON_MODAL_WINDOW
    }
}

export function closeMyModalWindow(): actions.CloseMyModalWindow {
    return {
        type: constants.CLOSE_MY_MODAL_WINDOW
    }
}

export function setIdOfEditTask(id): actions.SetIdOfEditTask {
    return {
        type: constants.SET_ID_OF_EDIT_TASK,
        payload: {
            id: id
        }
    }
}