import * as constantsLoginForm from '../constants/loginForm'
import * as constantsNav from '../constants/nav'
import * as actions from '../../types/NavActions'

export function setOpenedPage(page): actions.SetOpenedPage {
    return {
        type: constantsNav.SET_OPENED_PAGE,
        payload: {
            page: page
        }
    }
}

export function logout(): actions.Logout {
    return {
        type: constantsLoginForm.LOG_OUT
    }
}
