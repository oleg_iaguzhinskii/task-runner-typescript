import * as constants from '../constants/createCategory'
import * as actions from '../../types/CreateCategoryActions'

export function setTypeOfCreation(type): actions.TypeOfCreation {
    if (type === 'category') {
        return {
            type: constants.SET_TYPE_OF_CREATION,
            payload: {
                typeOfCreation: 'category'
            }
        }
    } else if (type === 'subcategory') {
        return {
            type: constants.SET_TYPE_OF_CREATION,
            payload: {
                typeOfCreation: 'subcategory'
            }
        }
    }
}

export function setFieldEmpty(): actions.EmptyField {
    return {
        type: constants.EMPTY_FIELD,
        payload: {
            errorMessage: 'Поля не могут быть пустыми!'
        }
    }
}

export function onChangeField(): actions.OnChangeField {
    return {
        type: constants.NOT_EMPTY_FIELD
    }
}

export function setTextTitle(text): actions.SetTextTitle {
    return {
        type: constants.SET_TEXT_TITLE,
        payload: {
            textTitle: text
        }
    }
}

export function setTextTextArea(text): actions.SetTextTextArea {
    return {
        type: constants.SET_TEXT_TEXT_AREA,
        payload: {
            textTextArea: text
        }
    }
}

export function selectCategory(category, subcategory): actions.SelectCategory {
    selectSubcategory(subcategory);

    return {
        type: constants.SET_CATEGORY,
        payload: {
            category: category
        }
    }
}

export function selectSubcategory(subcategory): actions.SelectSubCategory {
    return {
        type: constants.SET_SUBCATEGORY,
        payload: {
            subcategory: subcategory
        }
    }
}

export function resetForm(): actions.ResetForm {
    return {
        type: constants.RESET_FORM
    }
}

