import * as constants from '../constants/loginForm';
export function loginPending() {
    return {
        type: constants.LOGIN_PENDING
    };
}
function loginSuccess(json) {
    return {
        type: constants.LOGIN_SUCCESS,
        payload: {
            loggedUserLogin: json.login
        }
    };
}
function loginError(text) {
    return {
        type: constants.LOGIN_ERROR,
        payload: {
            errorMessage: text
        }
    };
}
export function login(login, password) {
    if (login && password) {
        login = login.toLowerCase();
        password = password.toLowerCase();
    }
    return function (dispatch) {
        dispatch(loginPending());
        return fetch('http://localhost:4000/users')
            .then(function (response) {
            return response.json();
        })
            .then(function (users) {
            let user = users.filter(u => u.login === login)[0];
            if (user && user.password === password) {
                dispatch(loginSuccess(user));
            }
            else {
                dispatch(loginError('Неправильно введён логин или пароль'));
            }
        })
            .catch(function (ex) {
            console.log('parsing failed: ' + ex);
        });
    };
}
export function handleLoginText(text) {
    text = text.toLowerCase();
    if (text.search(/[^0-9a-z]/i) !== -1) {
        return {
            type: constants.INVALID_LOGIN_TEXT,
            payload: {
                errorMessage: 'Поле логин должно состоять только из чисел или латинских символов'
            }
        };
    }
    else {
        return {
            type: constants.VALID_LOGIN_TEXT,
            payload: {
                errorMessage: null
            }
        };
    }
}
