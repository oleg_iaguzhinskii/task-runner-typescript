import * as constants from '../constants/myPage';
export function handleShowing(doesShow) {
    if (doesShow) {
        return {
            type: constants.SHOW_THE_COMPLETED_TASKS
        };
    }
    else {
        return {
            type: constants.HIDE_THE_COMPLETED_TASKS
        };
    }
}
