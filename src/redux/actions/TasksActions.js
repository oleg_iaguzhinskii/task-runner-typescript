import * as constants from '../constants/tasks';
export function openCommonModalWindow(id) {
    return {
        type: constants.OPEN_COMMON_MODAL_WINDOW,
        payload: {
            id: id
        }
    };
}
export function openMyModalWindow(id) {
    return {
        type: constants.OPEN_MY_MODAL_WINDOW,
        payload: {
            id: id
        }
    };
}
export function closeCommonModalWindow() {
    return {
        type: constants.CLOSE_COMMON_MODAL_WINDOW
    };
}
export function closeMyModalWindow() {
    return {
        type: constants.CLOSE_MY_MODAL_WINDOW
    };
}
export function setIdOfEditTask(id) {
    return {
        type: constants.SET_ID_OF_EDIT_TASK,
        payload: {
            id: id
        }
    };
}
