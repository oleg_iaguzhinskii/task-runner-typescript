export const LOGIN_PENDING = 'LOGIN_PENDING';
export type LOGIN_PENDING = typeof LOGIN_PENDING;

export const LOGIN_SUCCESS = 'LOGIN SUCCESS';
export type LOGIN_SUCCESS = typeof LOGIN_SUCCESS;

export const LOGIN_ERROR = 'LOGIN ERROR';
export type LOGIN_ERROR = typeof LOGIN_ERROR;

export const LOG_OUT = 'LOG_OUT';
export type LOG_OUT = typeof LOG_OUT;

export const INVALID_LOGIN_TEXT = 'INVALID_LOGIN_TEXT';
export type INVALID_LOGIN_TEXT = typeof INVALID_LOGIN_TEXT;

export const VALID_LOGIN_TEXT = 'VALID_LOGIN_TEXT';
export type VALID_LOGIN_TEXT = typeof VALID_LOGIN_TEXT;