export const EMPTY_FIELD = 'EMPTY_FIELD';
export type EMPTY_FIELD = typeof EMPTY_FIELD;

export const NOT_EMPTY_FIELD = 'NOT_EMPTY_FIELD';
export type NOT_EMPTY_FIELD = typeof NOT_EMPTY_FIELD;

export const SET_TEXT_TITLE_OF_TASK = 'SET_TEXT_TITLE_OF_TASK';
export type SET_TEXT_TITLE_OF_TASK = typeof SET_TEXT_TITLE_OF_TASK;

export const SET_TEXT_TEXT_AREA_OF_TASK = 'SET_TEXT_TEXT_AREA_OF_TASK';
export type SET_TEXT_TEXT_AREA_OF_TASK = typeof SET_TEXT_TEXT_AREA_OF_TASK;

export const SET_PRIVATE = 'SET_PRIVATE';
export type SET_PRIVATE = typeof SET_PRIVATE;

export const DELETE_PRIVATE = 'DELETE_PRIVATE';
export type DELETE_PRIVATE = typeof DELETE_PRIVATE;

export const SET_CATEGORY = 'SET_CATEGORY';
export type SET_CATEGORY = typeof SET_CATEGORY;

export const SET_SUBCATEGORY = 'SET_SUBCATEGORY';
export type SET_SUBCATEGORY = typeof SET_SUBCATEGORY;

export const SET_DATE_TILL = 'SET_DATE_TILL';
export type SET_DATE_TILL = typeof SET_DATE_TILL;

export const SET_HOURS_TO_REMIND = 'SET_HOURS_TO_REMIND';
export type SET_HOURS_TO_REMIND = typeof SET_HOURS_TO_REMIND;

export const SET_TAG = 'SET_TAG';
export type SET_TAG = typeof SET_TAG;

export const RESET_FORM = 'RESET_FORM';
export type RESET_FORM = typeof RESET_FORM;