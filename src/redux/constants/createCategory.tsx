export const EMPTY_FIELD = 'EMPTY_FIELD';
export type EMPTY_FIELD = typeof EMPTY_FIELD;

export const NOT_EMPTY_FIELD = 'NOT_EMPTY_FIELD';
export type NOT_EMPTY_FIELD = typeof NOT_EMPTY_FIELD;

export const SET_TEXT_TITLE = 'SET_TEXT_TITLE';
export type SET_TEXT_TITLE = typeof SET_TEXT_TITLE;

export const SET_TEXT_TEXT_AREA = 'SET_TEXT_TEXT_AREA';
export type SET_TEXT_TEXT_AREA = typeof SET_TEXT_TEXT_AREA;

export const SET_CATEGORY = 'SET_CATEGORY';
export type SET_CATEGORY = typeof SET_CATEGORY;

export const SET_SUBCATEGORY = 'SET_SUBCATEGORY';
export type SET_SUBCATEGORY = typeof SET_SUBCATEGORY;

export const SET_TYPE_OF_CREATION = 'SET_TYPE_OF_CREATION';
export type SET_TYPE_OF_CREATION = typeof SET_TYPE_OF_CREATION;

export const RESET_FORM = 'RESET_FORM';
export type RESET_FORM = typeof RESET_FORM;
