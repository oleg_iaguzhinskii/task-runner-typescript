import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/index';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
// import createSagaMiddleware from 'redux-saga';
// import rootSaga from "../sagas";

export default function configureStore(initialState) {
    const logger = createLogger();
    // const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk, logger));

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index');
            store.replaceReducer(nextRootReducer)
        })
    }

    // sagaMiddleware.run(rootSaga);

    return store
}