import * as constants from '../redux/constants/createCategory'

export interface TypeOfCreation {
    type: constants.SET_TYPE_OF_CREATION,
    payload: {
        typeOfCreation: string
    }
}

export interface EmptyField {
    type: constants.EMPTY_FIELD,
    payload: {
        errorMessage: string | null
    }
}

export interface OnChangeField {
    type: constants.NOT_EMPTY_FIELD
}

export interface SetTextTitle {
    type: constants.SET_TEXT_TITLE,
    payload: {
        textTitle: string
    }
}

export interface SetTextTextArea {
    type: constants.SET_TEXT_TEXT_AREA,
    payload: {
        textTextArea: string
    }
}

export interface SelectCategory {
    type: constants.SET_CATEGORY,
    payload: {
        category: string | undefined
    }
}

export interface SelectSubCategory {
    type: constants.SET_SUBCATEGORY,
    payload: {
        subcategory: string | undefined
    }
}

export interface ResetForm {
    type: constants.RESET_FORM
}