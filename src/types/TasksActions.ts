import * as constants from '../redux/constants/tasks'

export interface OpenedCommonModalWindow {
    type: constants.OPEN_COMMON_MODAL_WINDOW,
    payload: {
        id: number | null
    }
}

export interface OpenMyModalWindow {
    type: constants.OPEN_MY_MODAL_WINDOW,
    payload: {
        id: number | null
    }
}

export interface CloseCommonModalWindow {
    type: constants.CLOSE_COMMON_MODAL_WINDOW
}

export interface CloseMyModalWindow {
    type: constants.CLOSE_MY_MODAL_WINDOW
}

export interface SetIdOfEditTask {
    type: constants.SET_ID_OF_EDIT_TASK,
    payload: {
        id: number | null
    }
}