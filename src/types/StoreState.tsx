export interface CreateTaskStoreState {
    isFieldEmpty: boolean,
    textTitle: string,
    textTextArea: string,
    isPrivate: boolean,
    category: string | undefined,
    subcategory: string | undefined,
    dateTill: string,
    hoursToRemind: number,
    typeWordOfHours: string,
    tag: number,
    errorMessage: string | null
}

export interface CreateCategoryStoreState {
    typeOfCreation: string,
    isFieldEmpty: boolean,
    textTitle: string,
    textTextArea: string,
    category: string | undefined,
    subcategory: string | undefined,
    errorMessage: string | null
}

export interface LoginFormStoreState {
    isFetching: boolean,
    isLogged: boolean,
    errorMessage: string,
    loggedUserLogin: string | null,
    isLoginTextInvalid: boolean
}

export interface MyPageStoreState {
    doesShow: boolean
}

export interface NavStoreState {
    page: string
}

export interface SortingStoreState {
    sortBy: string,
    descending: boolean
}

export interface TasksStoreState {
    openedCommonModalWindow: number | null,
    openedMyModalWindow: number | null,
    idListOfCompletedTasks: number | null,
    id: number | null
}
