import * as constantsNav from '../redux/constants/nav'
import * as constantsLogin from '../redux/constants/loginForm'

export interface SetOpenedPage {
    type: constantsNav.SET_OPENED_PAGE,
    payload: {
        page: string
    }
}

export interface Logout {
    type: constantsLogin.LOG_OUT
}