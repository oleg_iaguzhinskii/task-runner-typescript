import * as constants from '../redux/constants/sorting'

export interface Sort {
    type: constants.SORT_TASK,
    payload: {
        sortBy: string,
        descending: boolean
    }
}