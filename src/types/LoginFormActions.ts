import * as constants from '../redux/constants/loginForm'

export interface LoginPending {
    type: constants.LOGIN_PENDING
}

export interface LoginSuccess {
    type: constants.LOGIN_SUCCESS,
    payload: {
        loggedUserLogin: string
    }
}

export interface LoginError {
    type: constants.LOGIN_ERROR,
    payload: {
        errorMessage: string | null
    }
}

export interface HandleLoginText {
    type: constants.INVALID_LOGIN_TEXT | constants.VALID_LOGIN_TEXT,
    payload: {
        errorMessage: string | null
    }
}