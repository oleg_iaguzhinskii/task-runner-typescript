import * as constants from '../redux/constants/myPage'

export interface HandleShowing {
    type: constants.SHOW_THE_COMPLETED_TASKS | constants.HIDE_THE_COMPLETED_TASKS
}