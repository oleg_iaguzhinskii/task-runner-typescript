import * as constants from '../redux/constants/createTask'

export interface EmptyField {
    type: constants.EMPTY_FIELD,
    payload: {
        errorMessage: string | null
    }
}

export interface OnChangeField {
    type: constants.NOT_EMPTY_FIELD
}

export interface SetTextTitleOfTask {
    type: constants.SET_TEXT_TITLE_OF_TASK,
    payload: {
        textTitle: string
    }
}

export interface SetTextTextAreaOfTask {
    type: constants.SET_TEXT_TEXT_AREA_OF_TASK,
    payload: {
        textTextArea: string
    }
}

export interface HandlePrivacy {
    type: constants.DELETE_PRIVATE | constants.SET_PRIVATE
}

export interface SelectCategory {
    type: constants.SET_CATEGORY,
    payload: {
        category: string | undefined
    }
}

export interface SelectSubCategory {
    type: constants.SET_SUBCATEGORY,
    payload: {
        subcategory: string | undefined
    }
}

export interface SelectDateTill {
    type: constants.SET_DATE_TILL,
    payload: {
        dateTill: string
    }
}

export interface SetHoursToRemind {
    type: constants.SET_HOURS_TO_REMIND,
    payload: {
        hoursToRemind: number,
        typeWordOfHours: string
    }
}

export interface SetTag {
    type: constants.SET_TAG,
    payload: {
        tag: number,
        errorMessage: string | null
    }
}

export interface ResetForm {
    type: constants.RESET_FORM
}